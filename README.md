**OVER HERE!**  
Version 2.0  
Auraes, 2020-2022  
Licence Creative Commons BY-NC-ND 4.0 (CC BY-NC-ND 4.0 FR)  
https://creativecommons.org/licenses/by-nc-nd/4.0/deed.fr

**Over Here!** est un jeu d’aventure textuel avec graphismes, réalisé à l’occasion de The Next Adventure Jam (juin 2020).  
Il a été développé avec le logiciel Adventuron, de Chris Ainsley.  
Les graphismes sont d'une résolution de 32 x 10 pixels ; la palette de 32 couleurs utilisée est la [hept32](https://lospec.com/palette-list/hept32).

Le jeu est jouable dans un navigateur, sur desktop ou mobile, à l’adresse suivante :  
https://auraes.itch.io/over-here

The Classic Adventures Solution Archive : [Indices et carte](http://www.solutionarchive.com/game/id%2C8663/Over+Here%21.html).

The Next Adventure Jam :  
https://itch.io/jam/next-adventure-jam  

Le logiciel Adventuron :  
https://adventuron.itch.io/  

Merci à Christopher Merriner, Beatchef, Scragglybear, Dan Fabulich et Rich H pour leurs conseils et remarques avisées. Ainsi qu’à tous ceux qui ont testé et commenté le jeu.
